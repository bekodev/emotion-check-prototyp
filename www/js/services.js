angular.module('emotions.factories', [])

    .factory('DataFactory', function () {
        return {
            settings: {
                apiUrl: 'https://westus.api.cognitive.microsoft.com/face/v1.0/detect?returnFaceAttributes=age,gender,emotion',
                apiKey: '8d9ce3677e3141d588da0ee2a6fae25b',
                pictureQuality: 100, //Qualit�t des verwendeten Bildes
                scoreMin: 12.5, //Mininum Score damit Emojis angezeigt werden 
                resolution: 1080
            },
            emotions: {
                anger: 'Wuetend',
                contempt: 'Verachtend',
                disgust: 'Empoert',
                fear: 'Aengstlich',
                happiness: 'Froehlich',
                neutral: 'Neutral',
                sadness: 'Traurig',
                surprise: 'Ueberrascht'
            },
            result: [
              {
                  "faceId": "38190471-c5f4-469a-9dce-b714dfa63803",
                  "faceRectangle": {
                      "width": 127,
                      "height": 127,
                      "left": 445,
                      "top": 75
                  },
                  "faceAttributes": {
                      "age": 48.8,
                      "gender": "male",
                      "headPose": {
                          "roll": -5.4,
                          "yaw": 10.5,
                          "pitch": 0
                      },
                      "smile": 1,
                      "facialHair": {
                          "moustache": 0.1,
                          "beard": 0,
                          "sideburns": 0
                      },
                      "glasses": "NoGlasses",
                      "emotion": {
                          "anger": 0,
                          "contempt": 0,
                          "disgust": 0,
                          "fear": 0,
                          "happiness": 1,
                          "neutral": 0,
                          "sadness": 0,
                          "surprise": 0
                      }
                  }
              },
              {
                  "faceId": "0095547b-5b76-46ce-9309-6d2ae43186fe",
                  "faceRectangle": {
                      "width": 121,
                      "height": 121,
                      "left": 442,
                      "top": 392
                  },
                  "faceAttributes": {
                      "age": 49.6,
                      "gender": "male",
                      "headPose": {
                          "roll": -5.9,
                          "yaw": 5.4,
                          "pitch": 0
                      },
                      "smile": 0,
                      "facialHair": {
                          "moustache": 0.2,
                          "beard": 0,
                          "sideburns": 0
                      },
                      "glasses": "NoGlasses",
                      "emotion": {
                          "anger": 0.858,
                          "contempt": 0.007,
                          "disgust": 0.002,
                          "fear": 0,
                          "happiness": 0,
                          "neutral": 0.131,
                          "sadness": 0.003,
                          "surprise": 0
                      }
                  }
              },
              {
                  "faceId": "b699336b-23eb-4e84-a7ba-f3f18438f280",
                  "faceRectangle": {
                      "width": 119,
                      "height": 119,
                      "left": 248,
                      "top": 376
                  },
                  "faceAttributes": {
                      "age": 55.3,
                      "gender": "male",
                      "headPose": {
                          "roll": -6,
                          "yaw": 10.1,
                          "pitch": 0
                      },
                      "smile": 0,
                      "facialHair": {
                          "moustache": 0.1,
                          "beard": 0,
                          "sideburns": 0
                      },
                      "glasses": "NoGlasses",
                      "emotion": {
                          "anger": 0.432,
                          "contempt": 0.025,
                          "disgust": 0.001,
                          "fear": 0,
                          "happiness": 0,
                          "neutral": 0.536,
                          "sadness": 0.005,
                          "surprise": 0
                      }
                  }
              },
              {
                  "faceId": "7541510b-ae43-4bf9-8c1b-8d76aec94d32",
                  "faceRectangle": {
                      "width": 116,
                      "height": 116,
                      "left": 46,
                      "top": 85
                  },
                  "faceAttributes": {
                      "age": 50,
                      "gender": "male",
                      "headPose": {
                          "roll": -7,
                          "yaw": 2,
                          "pitch": 0
                      },
                      "smile": 1,
                      "facialHair": {
                          "moustache": 0.1,
                          "beard": 0,
                          "sideburns": 0
                      },
                      "glasses": "NoGlasses",
                      "emotion": {
                          "anger": 0,
                          "contempt": 0,
                          "disgust": 0,
                          "fear": 0,
                          "happiness": 1,
                          "neutral": 0,
                          "sadness": 0,
                          "surprise": 0
                      }
                  }
              },
              {
                  "faceId": "9cb339c5-df5a-4d4f-aeda-ac30597bd2c4",
                  "faceRectangle": {
                      "width": 116,
                      "height": 116,
                      "left": 47,
                      "top": 378
                  },
                  "faceAttributes": {
                      "age": 51.1,
                      "gender": "male",
                      "headPose": {
                          "roll": -7.1,
                          "yaw": 0,
                          "pitch": 0
                      },
                      "smile": 0.001,
                      "facialHair": {
                          "moustache": 0.2,
                          "beard": 0,
                          "sideburns": 0
                      },
                      "glasses": "NoGlasses",
                      "emotion": {
                          "anger": 0.004,
                          "contempt": 0.004,
                          "disgust": 0,
                          "fear": 0,
                          "happiness": 0.001,
                          "neutral": 0.991,
                          "sadness": 0,
                          "surprise": 0
                      }
                  }
              },
              {
                  "faceId": "2e534531-0ae7-431e-96ba-5f3e5337d1de",
                  "faceRectangle": {
                      "width": 109,
                      "height": 109,
                      "left": 246,
                      "top": 77
                  },
                  "faceAttributes": {
                      "age": 48.9,
                      "gender": "male",
                      "headPose": {
                          "roll": -6,
                          "yaw": 11.7,
                          "pitch": 0
                      },
                      "smile": 1,
                      "facialHair": {
                          "moustache": 0.1,
                          "beard": 0,
                          "sideburns": 0
                      },
                      "glasses": "NoGlasses",
                      "emotion": {
                          "anger": 0,
                          "contempt": 0,
                          "disgust": 0,
                          "fear": 0,
                          "happiness": 1,
                          "neutral": 0,
                          "sadness": 0,
                          "surprise": 0
                      }
                  }
              }
            ]
        };
    })
    .factory('Utils', function () {
        return {
            // Base64-Image erstellen
            createBase64Image: function (source, x, y, width, height, callback) {
                var canvas = document.createElement('CANVAS');
                var ctx = canvas.getContext('2d');
                var img = new Image;

                img.crossOrigin = 'Anonymous';
                img.onload = function () {
                    var dataURL;
                    canvas.height = img.height;
                    canvas.width = img.width;
                    ctx.drawImage(img, x, y, width, height, 0, 0, canvas.width, canvas.height);

                    dataURL = canvas.toDataURL('image/jpeg');
                    callback.call(this, dataURL);
                    canvas = null;
                };

                img.src = source.src;
            },

            convertImgToBase64: function (url, outputFormat, callback) {
                var canvas = document.createElement('CANVAS');
                var ctx = canvas.getContext('2d');
                var img = new Image;

                img.crossOrigin = 'Anonymous';
                img.onload = function () {
                    var dataURL;
                    canvas.height = img.height;
                    canvas.width = img.width;
                    ctx.drawImage(img, 0, 0);
                    dataURL = canvas.toDataURL(outputFormat);
                    callback.call(this, dataURL);
                    canvas = null;
                };
                img.src = url;
            },

            dataURItoBlob: function (dataURI) {
                // convert base64 to raw binary data held in a string
                // doesn't handle URLEncoded DataURIs - see SO answer #6850276 for code that does this
                var byteString = atob(dataURI.split(',')[1]);

                // separate out the mime component
                var mimeString = dataURI.split(',')[0].split(':')[1].split(';')[0]

                // write the bytes of the string to an ArrayBuffer
                var ab = new ArrayBuffer(byteString.length);
                var ia = new Uint8Array(ab);
                for (var i = 0; i < byteString.length; i++) {
                    ia[i] = byteString.charCodeAt(i);
                }

                // write the ArrayBuffer to a blob, and you're done
                var blob = new Blob([ab], { type: mimeString });
                return blob;
            }
        }
    });