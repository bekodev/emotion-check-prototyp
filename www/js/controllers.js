﻿angular.module('emotions.controllers', [])
  .controller('PictureController', PictureController)
  .controller('SettingsController', SettingsController)
  .controller('ResultController', ResultController);

function PictureController($scope, $cordovaCamera, $http, $ionicLoading, $state, DataFactory, Utils) {
    $scope.settings = DataFactory.settings;

    $scope.getPictureFromCamera = function () {
        // TODO
    };

    $scope.getPictureFromGallery = function () {
        // TODO 
    };

    //Funktion für den API-Aufruf
    $scope.analyzePicture = function () {

        //Solange kein Bild ausgewählt wurde wird nicht fortgesetzt
        if ($scope.srcImage == null) {
            alert("Bitte wählen Sie zuerst ein Bild aus...");
            return;
        }
        
        //Ladeanimation wird angezeigt
        /* $ionicLoading.show({
            template: '<ion-spinner icon="spiral"></ion-spinner>'
        }); */

        console.log("API-Aufruf nicht implementiert");
    }

    function getPicture(sourceType) {
        // var options definieren 
        // => https://cordova.apache.org/docs/en/latest/reference/cordova-plugin-camera/index.html#take-a-picture-

        // Kamera-App starten und Optionen übergeben
        $cordovaCamera.getPicture(options).then(function (imageData) {
            $scope.srcImage = imageData;
        }, function (err) {
            console.log(JSON.stringify(err));
        });
    }

    function callEmotionsAPI(image) {
        Utils.convertImgToBase64(image, 'image/jpeg', function (base64Img) {
            // Wandelt die BASE64 codierten Bildaten in ein Blob um welches dann an die Emotion-APi gesendet wird
            var data = Utils.dataURItoBlob(base64Img);

            // Konfiguration für Web-Service Aufruf
            var config = {
                headers: {
                    'Content-Type': 'application/octet-stream', // Typ der übergebenen Daten
                    'ocp-apim-subscription-key': $scope.settings.apiKey //KEY für API
                }
            }

            //API-Aufruf
            $http.post($scope.settings.apiUrl, data, config).then(function (response) {
                console.log(JSON.stringify(response.data));

                if (response.data[0] == null) {
                    $ionicLoading.hide();
                    alert("Bild ungütlig: Es enthält keine verwertbaren Daten.");
                    return;
                }

                DataFactory.result = response.data;

                $ionicLoading.hide();
                $state.go('app.result', { reload: true });
            }, function (err) {
                console.log(JSON.stringify(err));

                $ionicLoading.hide();
                $state.go('app.result', { reload: true });
            });

        });
    }
  };

function SettingsController($scope, DataFactory) {
    $scope.settings = DataFactory.settings;
};

function ResultController($scope, DataFactory, Utils) {
    $scope.settings = DataFactory.settings;
    $scope.persons = DataFactory.result;
    $scope.emotions = DataFactory.emotions;

    var srcImage = document.querySelector('#srcImage');
    
    var canvas = document.getElementById("result-image");
    var context = canvas.getContext('2d');

    // Größe der Canvas-Fläche 
    canvas.setAttribute('width', srcImage.clientWidth);
    canvas.setAttribute('height', srcImage.clientHeight);
    
    // Faktor ermitteln um zwischen Original-Breite/Höhe des Bildes auf die tatsächliche Größe umzurechnen
    var factorW = srcImage.naturalWidth / canvas.clientWidth;
    var factorH = srcImage.naturalHeight / canvas.clientHeight;

    // Original-Bild auf die Größe des "result-image" zeichnen
    context.beginPath();
    context.drawImage(srcImage, 0, 0, canvas.clientWidth, canvas.clientHeight);    

    // Rechtecke um die erkannten Gesichter zeichnen
    angular.forEach($scope.persons, function (p, idx) {
        // TODO
    });

    // Gesichter der erkannten Person ausschneiden und als Bild speichern
    angular.forEach($scope.persons, function (p, idx) {
        // TODO
    });
};