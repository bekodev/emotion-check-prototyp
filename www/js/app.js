// Ionic Starter App
var app = angular.module('emotions', ['ionic', 'ngCordova', 'emotions.controllers', 'emotions.factories'])

  .run(function ($ionicPlatform) {
      $ionicPlatform.ready(function () {
          // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
          // for form inputs)
          if (window.cordova && window.cordova.plugins && window.cordova.plugins.Keyboard) {
              if (cordova.plugins.Keyboard.hideKeyboardAccessoryBar) {
                  cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
              }
              cordova.plugins.Keyboard.disableScroll(true);

          }
          if (window.StatusBar) {
              // org.apache.cordova.statusbar required
              StatusBar.styleDefault();
          }
      });
  })

  .config(function ($stateProvider, $urlRouterProvider) {
      $stateProvider

        .state('app', {
            url: '/app',
            abstract: true,
            templateUrl: 'templates/_layout.html'
        })
        .state('app.home', {
            url: '/home',
            views: {
                'menuContent': {
                    templateUrl: 'templates/home.html'
                }
            }
        })

      $urlRouterProvider.otherwise('/app/home');
  });

